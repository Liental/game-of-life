﻿using Life;

namespace Program
{
  class Program
  {
    static void Main()
    {
      Game life = new Game(Console.WindowWidth, Console.WindowHeight - 2);

      life.Initialize(500);

      while (true)
      {
        life.PrintGame();
        life.NextGeneration();
        Thread.Sleep(1000);
      }
    }
  }
}