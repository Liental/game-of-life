using Life.Geometric;

namespace Life
{
  class Cell
  {
    public Position Position;
    public bool IsAlive = false;

    public Cell(int x, int y)
    {
      this.Position = new Position(x, y);
    }

    public bool[] GetNeighbourState(bool[][] aliveTable)
    {
      List<bool> neighbours = new List<bool>();
      (int x, int y) = (this.Position.X, this.Position.Y);

      if (y > 0)
      {
        neighbours.Add(aliveTable[y - 1].ElementAtOrDefault(x - 1));
        neighbours.Add(aliveTable[y - 1][x]);
        neighbours.Add(aliveTable[y - 1].ElementAtOrDefault(x + 1));
      }

      if (y < aliveTable.Count() - 1)
      {
        neighbours.Add(aliveTable[y + 1].ElementAtOrDefault(x - 1));
        neighbours.Add(aliveTable[y + 1][x]);
        neighbours.Add(aliveTable[y + 1].ElementAtOrDefault(x + 1));
      }

      neighbours.Add(aliveTable[y].ElementAtOrDefault(x - 1));
      neighbours.Add(aliveTable[y].ElementAtOrDefault(x + 1));

      return neighbours.ToArray();
    }

    public void SetLifeState(bool[] neighbours)
    {
      bool[] aliveNeighbours = neighbours.Where(neighbour => neighbour).ToArray();
      int aliveCount = aliveNeighbours.Count();

      if (!this.IsAlive && aliveCount == 3)
      {
        this.IsAlive = true;
      }

      else if (this.IsAlive && aliveCount != 2 && aliveCount != 3)
      {
        this.IsAlive = false;
      }
    }
  }
}