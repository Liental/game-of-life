namespace Life.Geometric
{
  struct Position
  {
    public readonly int X;
    public readonly int Y;

    public Position(int x, int y) => (this.X, this.Y) = (x, y);
  }

  struct Size
  {
    public readonly int Width;
    public readonly int Height;

    public Size(int width, int height) => (this.Width, this.Height) = (width, height);
  }

}