namespace Life
{
  class Board : List<BoardRow>
  {
    public bool[][] GetAliveTable()
    {
      return this.Select(row => row.GetAliveRow()).ToArray();
    }

    public uint GetAliveCount()
    {
      uint alive = 0;

      foreach (BoardRow row in this) {
        alive += (uint) row.Where(cell => cell.IsAlive).Count();
      }

      return alive;
    }
  }

  class BoardRow : List<Cell>
  {
    public bool[] GetAliveRow()
    {
      return this.Select(cell => cell.IsAlive).ToArray();
    }
  }
}