using Life.Geometric;

namespace Life
{
  class Game
  {
    private readonly Random rand = new Random();
    private readonly Size Size;
    private uint Generation = 0;

    private Board Board = new Board();

    public Game(int width, int height)
    {
      this.Size = new Size(width, height);
    }

    public void Initialize(int numberAlive = 10)
    {
      for (int y = 0; y < this.Size.Height; y++)
      {
        BoardRow row = new BoardRow();

        for (int x = 0; x < this.Size.Width; x++)
        {
          Cell cell = new Cell(x, y);
          row.Add(cell);
        }

        this.Board.Add(row);
      }

      for (int i = 0; i < numberAlive; i++)
      {
        int row = this.rand.Next() % this.Size.Height;
        int column = this.rand.Next() % this.Size.Width;

        this.Board[row][column].IsAlive = true;
      }
    }

    public void NextGeneration()
    {
      bool[][] aliveTable = this.Board.GetAliveTable();

      for (int y = 0; y < this.Board.Count; y++)
      {
        BoardRow row = this.Board[y];

        for (int x = 0; x < row.Count; x++)
        {
          Cell cell = row[x];
          bool[] neighbours = cell.GetNeighbourState(aliveTable);

          cell.SetLifeState(neighbours);
        }
      }

      this.Generation++;
    }

    public void PrintGame()
    {
      string board = $"Generation: {this.Generation}\tAlive: {this.Board.GetAliveCount()}\n";

      for (int y = 0; y < this.Board.Count; y++)
      {
        BoardRow row = this.Board[y];

        for (int x = 0; x < row.Count; x++)
        {
          Cell cell = row[x];
          board += cell.IsAlive ? '#' : '-';
        }

        board += '\n';
      }

      Console.Clear();
      Console.Write(board);
    }
  }
}